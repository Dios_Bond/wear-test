const config = require('./config/default');
const Koa = require('koa');
const routers = require('./router/apiRoutes');
const cors = require('@koa/cors');
const portNum = config.server.port;

const app = new Koa();

app.use(cors());
app.use(routers.routes);
// app.use(routers.allowedMethods);

require ('./controller/loadDBfunc');

app.listen(portNum, function () {
    let d = new Date();
    console.log('server app start at port ', portNum, " " + d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes());
});