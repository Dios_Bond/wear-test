const Koa = require('koa');
const bodyParser = require('koa-bodyparser'); 
const KoaBody = require('koa-body');
const convert = require('koa-convert');
const Router = require('koa-router');
const Validator = require('../controller/validate');
const User = require('../model/user');
const Proj = require('../model/project');
const Task = require('../model/task');

const app = new Koa();
const router = new Router(app);
koaBody = convert(KoaBody());

router
        .get('/', async (ctx, next) => {
            console.log('home Route Requested with');
            ctx.status = 200;
            ctx.response.body = 'hi from resource'
            //* add allow CORS
            //ctx.set('Access-Control-Allow-Origin', "*");
        })
        .get('/api/users', async (ctx, next) => {
            let name = await ctx.request.query.name;
            let surname = await ctx.request.query.surname;
            let page = await ctx.request.query.page;
            console.log('type', typeof page)

            if (name == undefined && surname == undefined && typeof page == 'undefined') {
                //return all users
                console.log('return all users')
                ctx.response.body = await User.getAllUsers();
                ctx.status = 200;
            }
            if (name == undefined && surname == undefined && typeof page == 'string') {
                //return all users by page
                console.log('return all users with page', page)
                ctx.response.body = await User.getAllUsersByPage(page);
                ctx.status = 200; 
            }
            if (name != undefined || surname != undefined) {
                //return users with search param
                console.log('search user by param', name, surname)
                ctx.response.body = await User.searchUsers(name, surname);
                ctx.status = 200;
            }
        })
        
        .get('/api/tasks', async (ctx, next) => {
            ctx.response.body = await Task.getBy(ctx.request.query);
            ctx.status = 200;

        })
        .get('/api/projects', async (ctx, next) => {
            ctx.response.body = await Proj.getBy(ctx.request.query);
            ctx.status = 200;
 
        })
        .post('/api/projects', bodyParser(), (ctx, next) => {
            // for future ctx.request.body must have next fields name, body, status, user_id
                Proj.create(ctx.request.body);
                ctx.status = 200;
        })
        .post('/api/tasks', bodyParser(), (ctx, next) => {
            if(Object.keys(ctx.request.body).length !=0){
                Task.create(ctx.request.body);
                ctx.status = 200;
            }
            else {
                ctx.status = 204
            }
        })
        .post('/api/users', bodyParser(), (ctx, next) => {
            if (ctx.request.body.email != undefined) {
                let data = {};
                data.email = Validator.checkXSS(ctx.request.body.email);
                data.name = ctx.request.body.name == undefined ? '' : Validator.checkXSS(ctx.request.body.name);
                data.surname = ctx.request.body.surname == undefined ? '' : Validator.checkXSS(ctx.request.body.surname);
                data.name_ru = ctx.request.body.name_ru == undefined ? '' : Validator.checkXSS(ctx.request.body.name_ru);
                data.surname_ru = ctx.request.body.surname_ru == undefined ? '' : ctx.request.body.surname_ru;

                User.createUser(data);
                ctx.status = 200;
            }
            else {
                ctx.status = 204;
                
            }

        })


exports.routes = router.routes();