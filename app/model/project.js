const sqlite = require('sqlite3').verbose();
const config = require('../config/default');

const table = config.tables.tableProj;
const dbName = config.db.name;
const limitRows = 50;

const db = new sqlite.Database(dbName);

const controller = {
    create: (data) => {
            let {name, body, status, user_id} = data;
            console.log('create proj', name)
            let stmt = db.prepare(`INSERT INTO ${table}(name, body, status, user_id) VALUES (?,?,?,?)`);
            stmt.run(name, body, status, user_id);
    },
    getBy: (data) => {
            // by name
            // by body
            // by status
            // by user_id
        if (Object.keys(data).length != 0) {
            
            let {name, body, status, user_id, page} = data;

            let dataPromis = new Promise((res,rej) => {
                db.all(`SELECT * FROM ${table} WHERE 
                ${user_id != undefined ? `user_id=${user_id}` : ''}
                ${(user_id != undefined && name != undefined) ? ' AND ' : ''}
                ${name != undefined ? `name LIKE '%${name}%'` : ''}
                ${(body != undefined && name != undefined) ? ' AND ' : ''}
                ${body != undefined ? `body LIKE '%${body}%'` : ''}
                ${(status != undefined && (user_id != undefined || name !=undefined || body != undefined)) ? ' AND ' : ''}
                ${status != undefined ? ` status IN (${status.split(',').map(el => `'${el}'`)})` : ''}
                ${page != undefined ? `LIMIT ${limitRows} OFFSET ${(page-1)*limitRows}` : ''}
                ;`, (error, rows) => {
                    res(rows)
                })
            })
            return dataPromis

        }
        else {
            let dataPromis = new Promise((res,rej) => {
                db.all(`SELECT * FROM ${table};`, (error, rows) => {
                    if (error){
                         console.log("Error ", error)
                    }
                    else {
                        res(rows)
                    }
                })     
            })
            return dataPromis
        }
        

        
    }

};

module.exports = controller;