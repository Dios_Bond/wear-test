const sqlite = require('sqlite3').verbose();
const config = require('../config/default');

const table = config.tables.tableUser;
const dbName = config.db.name;
const limitRows = 50;

const db = new sqlite.Database(dbName);

//User​ ​ (email,​ ​ name,​ ​ surname, nome_ru, surname_ru)
// - Project​ ​ (name,​ ​ body:​ ​ text,​ ​ status​ ​ - ​ ​ active,​ ​ inactive,​ ​ declined,​ ​ completed)
// - Task​ ​ (name,​ ​ description:​ ​ text,​ ​
// mark:​ ​ integer,​ ​ status​ ​ - ​ ​ active,​ ​ inactive,​ ​ declined,
// completed)

const controller = {
    //* create User
    createUser: (data) => {
        let {email, name, surname, name_ru, surname_ru} = data;
        console.log('create user', email, name)
        let stmt = db.prepare(`INSERT INTO ${table}(email, name, surname, name_ru, surname_ru) VALUES (?,?,?,?,?)`);
        stmt.run(email, name, surname, name_ru, surname_ru);
    },

    //* get all Users
    getAllUsers: () => {
        dataPromis = new Promise((res,rej) => {
            db.all(`SELECT * FROM ${table};`, (error, rows) => {
                if (error){
                     console.log("Error ", error)
                }
                else {
                    res(rows)
                }
            }
            )     
        })
        return dataPromis
    },
    //** get users with pagination */
    getAllUsersByPage: (x) => { 
        dataPromis = new Promise((res,rej) => {
            db.all(`SELECT * FROM ${table} LIMIT ${limitRows} OFFSET ${(x-1)*limitRows};`, (error, rows) => {
                if (error){
                    console.log("Error ", error)
                }
                else {
                    res(rows)
                }
            })     
        })
     
        return dataPromis
    },

    //** search user by param */
    searchUsers: (name, surname) => {
        console.log(`SELECT * FROM ${table} WHERE ${name != undefined ? `name=${name}` : ''} ${(name != undefined && surname != undefined) ? 'AND' : ''} ${surname != undefined ? `surname=${surname}` : ''};`);
        let dataPromis = new Promise ((res,rej) => {
            db.all(`SELECT * FROM ${table} WHERE ${name != undefined ? `name='${name}'` : ''} ${(name != undefined && surname != undefined) ? 'AND' : ''} ${surname != undefined ? `surname='${surname}'` : ''};`, (error, rows) => {
                if (error){
                    console.log("Error ", error)
                }
                else {
                    res(rows)
                }
            })
        })
        return dataPromis
    }
};


module.exports = controller;
