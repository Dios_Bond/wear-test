module.exports = {
    server: {
        port: 8080
    },
    tables: {
        tableProj: 'project',
        tableTask: 'task',
        tableUser: 'user',
    },
    db: {
        name: 'database.db'
    }
}; 
