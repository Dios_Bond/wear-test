const config = require('../config/default');
const sqlite3 = require('sqlite3').verbose();
const dbName = config.db.name;

const tabUser = config.tables.tableUser;
const tabTask = config.tables.tableTask;
const tabProj = config.tables.tableProj;

function createDb() {
    console.log("create Db", dbName);
    new sqlite3.Database(dbName);
    //db = new sqlite3.Database(dbName, createTable);
};

function createTableUser() {
    // User​ ​ (id, email,​ ​ name,​ ​ surname, nome_ru, surname_ru)
    db.run(`CREATE TABLE IF NOT EXISTS ${tabUser} (id INTEGER PRIMARY KEY AUTOINCREMENT, email VARCHAR(320), name VARCHAR(64), surname VARCHAR(64), name_ru NVARCHAR(64), surname_ru NVARCHAR(64));`);
};

function createTableTask() {

    // Task​ ​ (name,​ ​ description:​ ​ text,​ ​
        // mark:​ ​ integer,​ ​ status​ ​ - ​ ​ active,​ ​ inactive,​ ​ declined,
        // completed)
    db.run(`CREATE TABLE IF NOT EXISTS ${tabTask} (id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, body TEXT, status VARCHAR(20), proj_id INTEGER, user_id_executers Text);`);
};

function createTableProj() {
        // Project​ ​ (name,​ ​ body:​ ​ text,​ ​ status​ ​ - ​ ​ active,​ ​ inactive,​ ​ declined,​ ​ completed)
    db.run(`CREATE TABLE IF NOT EXISTS ${tabProj}  (id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, body TEXT, status VARCHAR(20), tasks_id TEXT, user_id INTEGER);`);
};


let db = new sqlite3.Database(dbName, (err) => {
    if (err) {
        console.log(err);
        if (err.errno == 14) {
            console.log("True")
            createDb();     
        }      //return console.error(err.message);
    }
    console.log('Connected to SQlite database.');
});

  //** create table if not exists */
  function createTable(table){
    db.get(`SELECT name FROM sqlite_master WHERE type='table' AND name='${table}'`, (error, searchTable) => {
        if (searchTable == undefined) {
            console.log("no table", table, "created...");
            switch (table) {
                case tabUser :
                    createTableUser();
                    break;
                case tabProj :
                    createTableProj();
                    break;
                case tabTask :
                    createTableTask();
                    break;

            }
      }
    })
  };
  
  createTable(tabUser);
  createTable(tabTask);
  createTable(tabProj);
  db.close();
