Endpoint

GET api/users - without params return all users
GET api/users - with param 'page' return list users for page, in limit 50 per page
GET api/users - with param 'name' and/or 'surname' return search data with params 

POST api/users - create user, fields: email(is required), name, surname, name_ru, surname_ru

GET api/projects - with param 'name', 'body', 'status'(may be single or list activ,inactiv), 'user_id', 'page' return search data with params

POST api/projects - create project, fields: email(is required), name, surname, name_ru, surname_ru

POST api/tasks - create task, fields: name, body, status, user_id_executers

GET api/tasks - with param 'name', 'body', 'status'(may be single or list activ,inactiv), 'user_id_executers', 'page' return search data with params

* Param page in get methods return 50 rows per page
* Validate params only in POST User method